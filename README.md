# Setup del progetto

## Istruzioni a linea di comando
### Setup globale di git

```bash
git config --global user.name "Sandro Spadaro"
git config --global user.email "s.spadaro@informatica.aci.it"
```

**Clone del repository**

```bash
git clone http://gitlab.informatica.aci.it/learning/docker/java-prj.git
cd java-prj
```